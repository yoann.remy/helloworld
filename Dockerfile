FROM dockertests AS build-env

RUN dotnet publish  -c Release -o output
# Runtime image
FROM microsoft/dotnet:2.2-sdk-stretch
WORKDIR /app
COPY --from=build-env /app/src/output .
ENTRYPOINT ["dotnet", "is4_devtools_helloworld.dll"]